# GamificationMyAss

In my opinion, the internet is broken. One aspect that led to my conclusion is gamification. Gamification is the implementation of game-design elements like collecting points to improve user engagement.

![xkcd1566](https://imgs.xkcd.com/comics/board_game.png)

Websites like stackoverflow.com that has the wonderful basic idea of creating a quality collection of questions and answers that help developers across the world have implemented gamification aspects to pursue their idea. They implemented it with good intention but it leads in my opinion to unintended behavior and sometimes discouragement.

I know it is better to not get allured in the first place but I am tired of wasting my time figuring out that I got allured. This project contains user scripts for [Greasemonkey](https://wiki.greasespot.net/Main_Page) to make websites like stackoverflow.com a better place again. They remove gamification elements so that we can finally answer questions because they are interesting for us or simply because we want to help someone and not because we think that we need to earn some "reputation".